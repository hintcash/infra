Run: "docker-compose up --build" to start the frontend and the backend projects in the localenvironment.
The infra project needs to be in the same directory as the frontend and the backend.

backend:

 * locahost:8080
 * https://volks-test-backend.herokuapp.com/

frontend:

 * localhost:3000
 * https://volks-test-frontend.herokuapp.com/


 Obs: For the heroku environment, it can take a while to startup the application, both front and backend.
 The free version puts the application to sleep after a while without any request.